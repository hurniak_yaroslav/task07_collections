package com.hurnyakyaroslav.model;

public interface Model<K extends Comparable<K>, V> {
    int size();
    boolean isEmpty();
    V get(Object key);
    V remove(Object key);

}
