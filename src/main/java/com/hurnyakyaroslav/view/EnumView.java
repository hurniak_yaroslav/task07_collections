package com.hurnyakyaroslav.view;

import com.hurnyakyaroslav.controller.Controller;
import com.sun.xml.internal.ws.server.ServerRtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class EnumView implements View{
    Logger logger;
    private  Controller controller;
    private ArrayList<String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public enum MenuItem{

        PRINT(1), PUT(2), GET(3), REMOVE(4), EXIT(5);
        int number;
        MenuItem(int i) {
            number = i;
        }

        public int getNumber() {
            return number;
        }
    }

    public EnumView() {
        input = new Scanner(System.in);

        logger = LogManager.getLogger(MapView.class);
        controller = new Controller();


        menu = new ArrayList<>();
        menu.add("  1 - print tree");
        menu.add( "  2 - put element");
        menu.add( "  3 - get element");
        menu.add("  4 - remove element");
        menu.add( "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::print);
        methodsMenu.put("2", this::put);
        methodsMenu.put("3", this::get);
        methodsMenu.put("4", this::remove);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (MenuItem str : MenuItem.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (MenuItem.valueOf(keyMenu)){
                    case PRINT:
                        print();
                        break;
                    case GET:
                        get();
                        break;
                    case PUT:
                        put();
                        break;
                    case REMOVE:
                        remove();
                        break;
                    case EXIT:
                        return;
                    default:
                        logger.warn("Wrong input!");
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void print() {
        logger.info(controller.print());
    }


    private void put() {
        logger.info("Put string key and value to add: \n");
        logger.debug(controller.put(input.nextLine(), input.nextLine()));
    }

    private void get() {
        logger.info("Put string key to find: \n");
        logger.info(controller.get(input.nextLine()));
    }

    private void remove() {
        logger.info("Put string key to remove: \n");
        logger.info(controller.remove(input.nextLine()));
    }
}
